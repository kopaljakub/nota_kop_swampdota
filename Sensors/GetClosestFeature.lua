local sensorInfo = {
	name = "SafeUnits",
	desc = "Return positions of units which can be picked safely.",
	author = "PepeAmpere",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end



-- @description return units positions
return function(unit)
	local x1, y1, z1 = Spring.GetUnitPosition(unit)
	unitPosition = Vec3(x1, y1, z1)
	local minDistance = 9876543
	local closestFeature = {}
	closestFeature[1] = Vec3(0, 0, 0)
	closestFeature[2] = minDistance

	local features = Spring.GetAllFeatures()
	for i=1, #features do
		local x, y, z = Spring.GetFeaturePosition(features[i])
		featurePosition = Vec3(x, y, z)
		local distance = featurePosition:Distance(unitPosition)
		if distance < minDistance then
			closestFeature[1] = featurePosition
			closestFeature[2] = distance
			minDistance = distance
		end
	end
	return closestFeature
end