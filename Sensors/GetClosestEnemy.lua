local sensorInfo = {
	name = "SafeUnits",
	desc = "Return positions of units which can be picked safely.",
	author = "PepeAmpere",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end



-- @description return units positions
return function(box)
	-- not valid argument
	if box == -1 then
		return Vec3(0, 0, 0)
	end
	
	local closestEnemie = Spring.GetUnitNearestEnemy(box)
	if closestEnemie == nil then
		return Vec3(0, 0, 0)
	end
	local x, y, z = Spring.GetUnitPosition(closestEnemie) 
	return Vec3(x, y, z)
end