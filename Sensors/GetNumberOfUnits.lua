local sensorInfo = {
	name = "NumberOfUnits",
	desc = "Get number of specific unit type",
	author = "PepeAmpere",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


-- @description return units positions
return function(unitType)
	local numberOfUnits = 0
	local teamUnitIds = Spring.GetTeamUnits(Spring.GetMyTeamID())
	for i=1, #teamUnitIds do
		local unitDefId = Spring.GetUnitDefID(teamUnitIds[i])
		if UnitDefs[unitDefId].name == unitType then
			numberOfUnits = numberOfUnits + 1
		end
	end
	return numberOfUnits
end