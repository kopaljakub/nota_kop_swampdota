local sensorInfo = {
	name = "SafeUnits",
	desc = "Return positions of units which can be picked safely.",
	author = "PepeAmpere",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end



-- @description return units positions
return function(unitType)
	local teamUnitIds = Spring.GetTeamUnits(Spring.GetMyTeamID())
	for i=1, #teamUnitIds do
		local used = false
		for j=1, bb.usedIndex do
			if teamUnitIds[i] == bb.usingUnit[j] then
				used = true
			end
		end
		local unitDefId = Spring.GetUnitDefID(teamUnitIds[i])
		if used == false and UnitDefs[unitDefId].name == unitType then 
			local unitDefId = Spring.GetUnitDefID(teamUnitIds[i])
			bb.usingUnit[bb.usedIndex] = teamUnitIds[i]
			bb.usedIndex = bb.usedIndex + 1
			return teamUnitIds[i]	
		end
	end
	-- invalid value
	return -1
end