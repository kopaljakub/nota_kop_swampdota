local sensorInfo = {
	name = "SafeUnits",
	desc = "Return positions of units which can be picked safely.",
	author = "PepeAmpere",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end



-- @description return units positions
return function(unit)
	local x1, y1, z1 = Spring.GetUnitPosition(unit)
	unitPosition = Vec3(x1, y1, z1)
	local closestFeature = Vec3(0, 0, 0)
	local minDistance = 9876543

	-- local features = Spring.GetAllFeatures()
	local teamUnitIds = Spring.GetTeamUnits(Spring.GetMyTeamID())
	for i=1, #teamUnitIds do
		local health, maxHealth, q1, q2, q3 = Spring.GetUnitHealth(teamUnitIds[i])
		if health < maxHealth then
			local x, y, z = Spring.GetUnitPosition(teamUnitIds[i])
			featurePosition = Vec3(x, y, z)
			local distance = featurePosition:Distance(unitPosition)
			if distance < minDistance then
				closestFeature = featurePosition
				minDistance = distance
			end
		end
	end
	return closestFeature
end