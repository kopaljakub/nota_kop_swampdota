local sensorInfo = {
	name = "SafeUnits",
	desc = "Return positions of units which can be picked safely.",
	author = "PepeAmpere",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end



-- @description return units positions
return function(unitFrom, unitType)
	local x1, y1, z1 = Spring.GetUnitPosition(unitFrom)
	local fromPosition = Vec3(x1, y1, z1)
	local closestUnit = -1
	local minDistance = 9876543

	local teamUnitIds = Spring.GetTeamUnits(Spring.GetMyTeamID())
	for i=1, #teamUnitIds do
		local used = false
		for j=1, bb.usedIndex do
			if teamUnitIds[i] == bb.usingUnit[j] then
				used = true
			end
		end

		local inBattle = false
		for j=1, #bb.InBattle do
			if teamUnitIds[i] == bb.InBattle[j] then
				inBattle = true
			end
		end

		local unitDefId = Spring.GetUnitDefID(teamUnitIds[i])
		if used == false and inBattle == false and UnitDefs[unitDefId].name == unitType then 
			local x, y, z = Spring.GetUnitPosition(teamUnitIds[i])
			unitPosition = Vec3(x, y, z)
			local distance = unitPosition:Distance(fromPosition)
			if distance < minDistance then
				closestUnit = teamUnitIds[i]
				minDistance = distance
			end
		end
	end
	-- register used unit
	if closestUnit > -1 then
		bb.usingUnit[bb.usedIndex] = closestUnit
		bb.usedIndex = bb.usedIndex + 1
	end
	return closestUnit
end