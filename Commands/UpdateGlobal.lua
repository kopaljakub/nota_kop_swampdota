function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "info",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end



local function ClearState(self)
	self.running=false
end



function Run(self, units, parameter)
	mission = parameter.info

	bb.ever = mission
	if bb.final == false then

		bb.lineMoved = false
		local oldEnemy = #bb.enemyPoints[2]
		UpdateCoridors(mission)
		local newEnemy = #bb.enemyPoints[2]

		if #bb.enemyPoints[2] == 0 then
			bb.enemyPoints[2][1] = bb.myPoints[2][#bb.myPoints[2]]
			bb.final = true
		end

		-- line moved
		if newEnemy < oldEnemy then
			bb.InBattle = {}
			bb.lineMoved = true
		end
	end


	return SUCCESS
end



function Reset(self)
	ClearState(self)
end


function UpdateCoridors(mission)
	local points = mission.corridors.Top.points	
	bb.enemyPoints[1] = {}
	for i=1, #points do
		if points[i].isStrongpoint == true and points[i].ownerAllyID == 1 then
			bb.enemyPoints[1][#bb.enemyPoints[1] + 1] = points[i].position
		end
	end
	local points = mission.corridors.Middle.points	
	bb.enemyPoints[2] = {}
	for i=1, #points do
		if points[i].isStrongpoint == true and points[i].ownerAllyID == 1 then
			bb.enemyPoints[2][#bb.enemyPoints[2] + 1] = points[i].position
		end
	end
	local points = mission.corridors.Bottom.points	
	bb.enemyPoints[3] = {}
	for i=1, #points do
		if points[i].isStrongpoint == true and points[i].ownerAllyID == 1 then
			bb.enemyPoints[3][#bb.enemyPoints[3] + 1] = points[i].position
		end
	end

	local points = mission.corridors.Top.points	
	bb.myPoints[1] = {}
	for i=1, #points do
		if points[i].isStrongpoint == true and points[i].ownerAllyID == 0 then
			bb.myPoints[1][#bb.myPoints[1] + 1] = points[i].position
		end
	end
	local points = mission.corridors.Middle.points	
	bb.myPoints[2] = {}
	for i=1, #points do
		if points[i].isStrongpoint == true and points[i].ownerAllyID == 0 then
			bb.myPoints[2][#bb.myPoints[2] + 1] = points[i].position
		end
	end
	local points = mission.corridors.Bottom.points	
	bb.myPoints[3] = {}
	for i=1, #points do
		if points[i].isStrongpoint == true and points[i].ownerAllyID == 0 then
			bb.myPoints[3][#bb.myPoints[3] + 1] = points[i].position
		end
	end
end
