function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "squadId",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end



local function ClearState(self)
	self.running=false
end



function Run(self, units, parameter)
	local squadId = parameter.squadId



	local readyToMine = 0
	-- sign for Miners to start mining
	for i=1, bb.squadSize do
		-- unit is valid
		if not (bb.minerSquad[squadId].unit[i] == -1 or Spring.ValidUnitID(bb.minerSquad[squadId].unit[i]) == false) then
			local health, maxHealth, q1, q2, q3 = Spring.GetUnitHealth(bb.minerSquad[squadId].unit[i])
			if health < maxHealth then
				return SUCCESS
			end
			readyToMine = readyToMine + 1
		end
	end
	if readyToMine > 2 then
		bb.minerSquad[squadId].Reclaim = true
	end
	return SUCCESS
end



function Reset(self)
	ClearState(self)
end


