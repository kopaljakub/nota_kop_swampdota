function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "unit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "squadId",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end



local function ClearState(self)
	self.running=false
end



function Run(self, units, parameter)
	unit = parameter.unit
	squadId = parameter.squadId

	if unit == -1 then
		return SUCCESS
	end

	for i=1, bb.squadSize do
		if bb.minerSquad[squadId].unit[i] == -1 then
			bb.minerSquad[squadId].unit[i] = unit
			return SUCCESS
		end
	end
end



function Reset(self)
	ClearState(self)
end

