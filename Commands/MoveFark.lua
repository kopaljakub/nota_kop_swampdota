function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "fark",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}

		}
	}
end



local function ClearState(self)
	self.running=false
end



function Run(self, units, parameter)
	local fark = parameter.fark
	local targetPosition = bb.myPoints[2][#bb.myPoints[2] - 1]
	local target = targetPosition:AsSpringVector()

	-- not valid units. Unregister if needed
	if fark == -1 or fark == nil then
		return SUCCESS
	end

	-- not valid units. Unregister if needed
	if Spring.ValidUnitID(fark) == false then
		for i=1, bb.usedIndex do
			if bb.usingUnit[i] == fark then
				bb.usingUnit[i] = nil
			end
		end
		return SUCCESS
	end




	-- -- assign command
	-- if not self.running then
	-- 	Spring.GiveOrderToUnit(radar, CMD.MOVE, {target[1], target[2], target[3]}, {"alt"})
	-- 	self.running = true
	-- end

	Spring.GiveOrderToUnit(fark, CMD.RECLAIM, {target[1], target[2], target[3], 1500}, {"alt"})

	return RUNNING


	-- -- unit has commands to do
	-- local temp = Spring.GetUnitCommands(attackingUnit, 0)
	-- if temp > 0 or init == true then
	-- 	return RUNNING
	-- end


	-- for i=1, bb.usedIndex do
	-- 	if bb.usingUnit[i] == attackingUnit then
	-- 		bb.usingUnit[i] = nil
	-- 		-- return SUCCESS
	-- 	end
	-- end
	-- return SUCCESS

	
	
end


function Reset(self)
	ClearState(self)
end
