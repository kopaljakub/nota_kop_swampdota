function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "attackingUnit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end



local function ClearState(self)
	self.running=false
	self.enemyNumber = 0
end



function Run(self, units, parameter)
	local attackingUnit = parameter.attackingUnit
	local targetPosition = bb.enemyPoints[2][1]
	local target = targetPosition:AsSpringVector()

	-- not valid units. Unregister if needed
	if attackingUnit == -1 or attackingUnit == nil or Spring.ValidUnitID(attackingUnit) == false then
		for i=1, bb.usedIndex do
			if bb.usingUnit[i] == attackingUnit then
				bb.usingUnit[i] = nil
			end
		end
		return SUCCESS
	end

	-- register unit if for first time
	if not self.running then
		self.enemyNumber = #bb.enemyPoints[2]
		self.running = true
	end


	local unitDefId = Spring.GetUnitDefID(attackingUnit)
	local weaponDefId = UnitDefs[unitDefId].weapons[1].weaponDef
	local range = WeaponDefs[weaponDefId].range

	local closestEnemie = Spring.GetUnitNearestEnemy(attackingUnit)
	local closest = 987654
	if closestEnemie ~= nil then
		local x, y, z = Spring.GetUnitPosition(closestEnemie) 
		local x1, y1, z1 = Spring.GetUnitPosition(attackingUnit) 
		local pos = Vec3(x, y, z)
		local pos1 = Vec3(x1, y1, z1)
		closest = pos:Distance(pos1)
	end



	-- lined moved
	if self.enemyNumber > #bb.enemyPoints[2] then
		for i=1, bb.usedIndex do
			if bb.usingUnit[i] == attackingUnit then
				bb.usingUnit[i] = nil
			end
		end
		for i=1, #bb.InBattle do
			if bb.InBattle[i] == attackingUnit then
				bb.InBattle[i] = -1
			end
		end
		return SUCCESS
	end

	local unitsToAttack = Spring.GetUnitsInSphere(target[1], target[2], target[3], bb.stronPointRadius)
	-- enemy is too close or no enemie units in strongPoint
	if closest < (range / 4)  or #unitsToAttack == 0 then
		Spring.GiveOrderToUnit(attackingUnit, CMD.ATTACK, {x, y, z}, {"alt"})
	else
		local tX, tY, tZ = Spring.GetUnitPosition(unitsToAttack[1])
		Spring.GiveOrderToUnit(attackingUnit, CMD.ATTACK, {tX, tY, tZ}, {"alt"})
	end


	return RUNNING	
	
end


function Reset(self)
	ClearState(self)
end
