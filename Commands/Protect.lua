function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "unit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "squadId",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "corridor",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end


-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.running=false
end



function Run(self, units, parameter)
	local unit = parameter.unit
	local squadId = parameter.squadId
	local corridor = parameter.corridor

	-- unit doesnt exitst
	if unit == -1 or unit == nil then
		return SUCCESS
	end

	-- unit died
	if Spring.ValidUnitID(unit) == false then
		-- from squad
		for i=1, bb.squadSize do
			if bb.minerSquad[squadId].unit[i] == unit then
				bb.minerSquad[squadId].unit[i] = -1
			end
		end
		-- from using units
		for i=1, bb.usedIndex do
			if bb.usingUnit[i] == unit then
				bb.usingUnit[i] = nil
			end
		end
		return SUCCESS
	end



	local health, maxHealth, q1, q2, q3 = Spring.GetUnitHealth(unit)


	-- sign to bacp up
	if health < 0.7 * maxHealth then
		bb.minerSquad[squadId].Reclaim = false
	end

	-- not assign command
	if bb.minerSquad[squadId].Reclaim == true then
		local target = bb.enemyPoints[corridor][1]:AsSpringVector()
		local closestEnemie = Spring.GetUnitNearestEnemy(unit)

		-- if closest doent exist or is too far away, attack base
		if closestEnemie == nil or distance(unit, closestEnemie) > 1000 then
			local target = bb.enemyPoints[corridor][1]:AsSpringVector()
			Spring.GiveOrderToUnit(unit, CMD.ATTACK, {target[1], target[2], target[3], 2000}, {"alt"})
		else
			local x, y, z = Spring.GetUnitPosition(closestEnemie) 
			Spring.GiveOrderToUnit(unit, CMD.ATTACK, {x, y, z, 500}, {"alt"})
		end
	else
		local target = bb.myPoints[corridor][#bb.myPoints[corridor]]:AsSpringVector()
		Spring.GiveOrderToUnit(unit, CMD.MOVE, {target[1], target[2], target[3], 300}, {"alt"})
	end		
	return RUNNING
	
end


function Reset(self)
	ClearState(self)
end


function Unregistry(unit)
	for i=1, bb.usedIndex do
		if bb.usingUnit[i] == unit then
			bb.usingUnit[i] = nil
		end
	end
end



function distance(unit, unitTo)
	local x, y, z = Spring.GetUnitPosition(unit) 
	local x1, y1, z1 = Spring.GetUnitPosition(unitTo) 
	local pos = Vec3(x, y, z)
	local pos1 = Vec3(x1, y1, z1)
	return pos:Distance(pos1)
end