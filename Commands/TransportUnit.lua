function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "transporter",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "unitToTransport",
				variableType = "table",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end



local function ClearState(self)
	self.running=false
	self.addedRecord = false
end



function Run(self, units, parameter)
	bb.fail = false
	local transporter = parameter.transporter 
	local unitToTransport = parameter.unitToTransport
	local targetPosition = bb.enemyPoints[2][1]
	local target = targetPosition:AsSpringVector()
	
	local xOffset = -700
	local yOffset = -450
	if #bb.enemyPoints[2] == 1 then
		xOffset = -750
		yOffset = -100
	end


	local basePosition = bb.myPoints[2][#bb.myPoints[2] - 1]
	local base = basePosition:AsSpringVector()

	-- not valid units
	if unitToTransport == -1 or transporter == -1 then
		if unitToTransport > -1 then
			for i=1, bb.usedIndex do
				if bb.usingUnit[i] == unitToTransport then
					bb.usingUnit[i] = nil
				end
			end
		end
		if transporter > -1 then
			for i=1, bb.usedIndex do
				if bb.usingUnit[i] == transporter then
					bb.usingUnit[i] = nil
				end
			end
		end		
		return SUCCESS
	end


	local init = false
	if not self.running then
		local sourceX, sourceY, sourceZ = Spring.GetUnitPosition(unitToTransport)
		Spring.GiveOrderToUnit(transporter, CMD.LOAD_UNITS, {sourceX, sourceY, sourceZ, 100}, {"shift"})
		Spring.GiveOrderToUnit(transporter, CMD.UNLOAD_UNITS, {target[1] + xOffset, target[2], target[3] + yOffset, 200}, {"shift"})
		Spring.GiveOrderToUnit(transporter, CMD.MOVE, {base[1], base[2], base[3], 1000}, {"shift"})
		init = true
		self.running = true
	end

	-- something died
	if Spring.ValidUnitID(transporter) == false or Spring.ValidUnitID(unitToTransport) == false then
		for i=1, bb.usedIndex do
			if bb.usingUnit[i] == transporter or bb.usingUnit[i] == unitToTransport then
				bb.usingUnit[i] = nil
			end
		end
		return SUCCESS
	end

	-- unit has commands to do
	local cmdNumber = Spring.GetUnitCommands(transporter, 0)
	if self.addedRecord == false and cmdNumber == 1 then
		bb.InBattle[#bb.InBattle + 1] = unitToTransport
		self.addedRecord = true
	end
	if cmdNumber > 0 or init == true then
		return RUNNING
	end



	-- Mission completed
	-- remove unit from using units table
	for i=1, bb.usedIndex do
		if bb.usingUnit[i] == transporter or bb.usingUnit[i] == unitToTransport then
			bb.usingUnit[i] = nil
		end
	end		
	bb.InBattle[#bb.InBattle + 1] = unitToTransport
	return SUCCESS
	
end


function Reset(self)
	ClearState(self)
end
