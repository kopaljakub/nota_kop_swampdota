function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "squadId",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end



local function ClearState(self)
	self.running=false
end



function Run(self, units, parameter)
	squadId = parameter.squadId

	bb.minerSquad[squadId] = {}
	bb.minerSquad[squadId].unit = {}
	for i=1,bb.squadSize do
		bb.minerSquad[squadId].unit[i] = -1
	end

	bb.minerSquad[squadId].Reclaim = true


	return SUCCESS
end



function Reset(self)
	ClearState(self)
end

