function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "unit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "squadId",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "corridor",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end


-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.running=false
	self.move = false
end



function Run(self, units, parameter)
	local unit = parameter.unit
	local squadId = parameter.squadId
	local corridor = parameter.corridor

	-- unit doesnt exitst
	if unit == -1 or unit == nil then
		return SUCCESS
	end

	-- unit died
	if Spring.ValidUnitID(unit) == false then
		-- from squad
		for i=1, bb.squadSize do
			if bb.minerSquad[squadId].unit[i] == unit then
				bb.minerSquad[squadId].unit[i] = -1
			end
		end
		-- from using units
		for i=1, bb.usedIndex do
			if bb.usingUnit[i] == unit then
				bb.usingUnit[i] = nil
			end
		end
		return SUCCESS
	end


	local health, maxHealth, q1, q2, q3 = Spring.GetUnitHealth(unit)


	-- sign to bacp up
	if health < 0.7 * maxHealth then
		bb.minerSquad[squadId].Reclaim = false
	end

	-- not assign command
	if bb.minerSquad[squadId].Reclaim == true then
		local target = bb.enemyPoints[corridor][1]:AsSpringVector()
		--Spring.GiveOrderToUnit(unit, CMD.RECLAIM, {target[1], target[2], target[3], 1000}, {"alt"})
		Spring.GiveOrderToUnit(unit, CMD.RECLAIM, {target[1], target[2], target[3], 2000}, {"alt"})
		self.move = false
	else
		local cmdNumber = Spring.GetUnitCommands(unit, 0)
		if self.move == false or cmdNumber == 0 then
			local target = bb.myPoints[corridor][#bb.myPoints[corridor]]:AsSpringVector()
			Spring.GiveOrderToUnit(unit, CMD.MOVE, {target[1] - 30, target[2], target[3] + 30, 150}, {"alt"})
			Spring.GiveOrderToUnit(unit, CMD.REPAIR, {target[1], target[2], target[3], 200}, {"shift"})
			self.move = true
		end
	end		
	return RUNNING
	
end


function Reset(self)
	ClearState(self)
end


function Unregistry(unit)
	for i=1, bb.usedIndex do
		if bb.usingUnit[i] == unit then
			bb.usingUnit[i] = nil
		end
	end
end
