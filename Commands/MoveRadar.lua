function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "radar",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}

		}
	}
end



local function ClearState(self)
	self.running=false
end



function Run(self, units, parameter)
	local radar = parameter.radar
	local targetPosition = bb.myPoints[2][#bb.myPoints[2]]
	local target = targetPosition:AsSpringVector()

	-- not valid units. Unregister if needed
	if radar == -1 or radar == nil then
		return SUCCESS
	end

	-- not valid units. Unregister if needed
	if Spring.ValidUnitID(radar) == false then
		for i=1, bb.usedIndex do
			if bb.usingUnit[i] == radar then
				bb.usingUnit[i] = nil
			end
		end
		return SUCCESS
	end


	Spring.GiveOrderToUnit(radar, CMD.MOVE, {target[1] - 200, target[2], target[3] - 200}, {"alt"})

	return RUNNING

end


function Reset(self)
	ClearState(self)
end
